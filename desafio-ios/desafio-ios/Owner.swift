//
//  Owner.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 10/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class Owner: NSObject {
    
    var avatar_url:String?
    var id:Int?
    var login:String?
    var url:String?

}
