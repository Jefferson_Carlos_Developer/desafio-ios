//
//  Repository.swift
//  Desafio-iOS
//
//  Created by Jefferson Carlos Souza da Silva on 10/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class Repository: NSObject {
    
    var incomplete_results:Int?
    var total_count:Int?
    var item:[RepositoryItem]?
}
