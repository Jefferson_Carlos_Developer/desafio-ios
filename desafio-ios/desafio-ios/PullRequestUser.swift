//
//  PullRequestUser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class PullRequestUser: NSObject {
    
    var login:String?
    var id:Int?
    var avatar_url:String?
    
}
