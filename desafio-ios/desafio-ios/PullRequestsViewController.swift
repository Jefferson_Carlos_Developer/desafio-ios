//
//  PullRequestsViewController.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import Alamofire

class PullRequestsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:@IBOutlets
    @IBOutlet weak var tvPullRequests: UITableView!
    
    //MARK:Propoerties
    var user:String!
    var repository:String!
    
    private let active = UIActivityIndicator()
    private var arrayData:[PullRequest]!
    private var pullRequestParser: PullRequestParser!
    
    //MARK:LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.arrayData = [PullRequest]()
        self.pullRequestParser = PullRequestParser()

        self.tvPullRequests.delegate = self
        self.tvPullRequests.dataSource = self
        
        //Remove empty cells
        self.tvPullRequests.tableFooterView = UIView()

        self.loadContent()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:Functions
    func loadContent() {
        
        self.active.showActivityIndicator((self.navigationController?.view)!)
        
        let repositoryTask = PullRequestsTask()
        
        repositoryTask.retrieveRepositoriesHandler(user, repositoryName: repository) { (status, result, error) in
            
            self.active.hideActivityIndicator((self.navigationController?.view)!)
            
            if (status == true) {
                
                if(result?.count > 0){
                    
                    self.arrayData = result
                    self.tvPullRequests.reloadData()
                    
                }else{
                    
                    Util.showAlert(self,title: NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String,message: "Não foram encontrados registros!\nTente Novamente mais tarde.")
                    
                }
                
            }else{
                
                if(error != nil){
                    Util.showAlert(self,title: NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String,message: "Houve um problema!\nTente Novamente mais tarde.")
                }
            }
        }
    }
    
    //MARK: UITableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.arrayData != nil){
            return self.arrayData.count
        }else{
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "cellPullRequest"
        let pullRequest = self.arrayData[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! PullRequestsTableViewCell
        cell.configureCellWithPullRequest(pullRequest)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequest = self.arrayData[indexPath.row]
        Util.makeCommunication(pullRequest.html_url!)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.contentView.layer.opacity = 0.0
        UIView.animateWithDuration(0.5) {
            cell.contentView.layer.opacity = 1
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
