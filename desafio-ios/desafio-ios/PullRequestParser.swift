//
//  PullRequestParser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class PullRequestParser: NSObject {
    
    func parserPullRequestObject(pullRequest: AnyObject ) -> PullRequest{
        
        let myPullRequest = PullRequest()
        
        if let id = pullRequest.objectForKey("id"){
            myPullRequest.id = id as? Int
        }
        
        if let title = pullRequest.objectForKey("title"){
            
            print(title)
            myPullRequest.title = title as? String
        }
        
        if let body = pullRequest.objectForKey("body"){
            myPullRequest.body = body as? String
        }
        
        if let updated_at = pullRequest.objectForKey("updated_at"){
            myPullRequest.updated_at = updated_at as? String
        }
        
        if let html_url = pullRequest.objectForKey("html_url"){
            myPullRequest.html_url = html_url as? String
        }
        
        if let user = pullRequest.objectForKey("user"){
            myPullRequest.user = PullRequestUserParser().parserPullRequestObject(user)
        }
        
        
        
        return myPullRequest
    }
    

}
