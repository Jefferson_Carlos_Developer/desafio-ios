//
//  RepositoryTableViewCell.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    //Properties
    var repositoryItem:RepositoryItem!
    
    //@IBOutlets
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblRepositoryName: UILabel!
    @IBOutlet weak var lblReporitoryDescription: UILabel!
    @IBOutlet weak var lblTotalForks: UILabel!
    @IBOutlet weak var lblTotalStars: UILabel!
    @IBOutlet weak var aiLoader: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithRepositoryItem(repositoryItem:RepositoryItem){
        
        self.repositoryItem = repositoryItem
        self.lblUsername.text = repositoryItem.owner?.login
        self.lblTotalForks.text = String(repositoryItem.forks_count!)
        self.lblTotalStars.text = String(repositoryItem.stargazers_count!)
        self.lblReporitoryDescription.text = repositoryItem.itemDescription!
        self.lblRepositoryName.text = repositoryItem.name!.uppercaseString
        
        self.imgUser.image = nil
        
        self.aiLoader.startAnimating()
        self.aiLoader.hidden = false

        Util.loadImageFromStringUrl((repositoryItem.owner?.avatar_url)!, completion: { (imageView) in
            
            self.aiLoader.stopAnimating()
            self.aiLoader.hidden = true

            dispatch_async(dispatch_get_main_queue(), {

                self.imgUser.image = imageView.image
            })
            
        })
        
    }

}
