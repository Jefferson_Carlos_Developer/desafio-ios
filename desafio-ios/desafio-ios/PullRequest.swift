//
//  PullRequest.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class PullRequest: NSObject {
    
    var id:Int?
    var title:String?
    var user:PullRequestUser?
    var body:String?
    var updated_at:String?
    var html_url:String?
    
}
