//
//  RepositoryItemParser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 10/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class RepositoryItemParser: NSObject {
    
    func parserRepositoryItemObject(repositoryItem: AnyObject ) -> RepositoryItem{
        
        let myRepositoryItem = RepositoryItem()
        
        if let itemDescription = repositoryItem.objectForKey("description"){
            myRepositoryItem.itemDescription = itemDescription as? String
            
            if(myRepositoryItem.itemDescription == nil){
                myRepositoryItem.itemDescription = ""
            }
            
        }
        
        if let forks_count = repositoryItem.objectForKey("forks_count"){
            myRepositoryItem.forks_count = forks_count as? Int
        }
        
        if let id = repositoryItem.objectForKey("id"){
            myRepositoryItem.id = id as? Int
        }
        
        if let name = repositoryItem.objectForKey("name"){
            myRepositoryItem.name = name as? String
        }
        
        if let owner = repositoryItem.objectForKey("owner"){
            myRepositoryItem.owner = OwnerParser().parserOwnerObject(owner)
        }
        
        if let stargazers_count = repositoryItem.objectForKey("stargazers_count"){
            myRepositoryItem.stargazers_count = stargazers_count as? Int
        }
        
        if let url = repositoryItem.objectForKey("url"){
            myRepositoryItem.url = url as? String
        }
        

        
        return myRepositoryItem
    }

}
