//
//  OwnerParser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class OwnerParser: NSObject {
    
    func parserOwnerObject(owner: AnyObject ) -> Owner{

        let myOwner = Owner()
        
        if let avatar_url = owner.objectForKey("avatar_url"){
            myOwner.avatar_url = avatar_url as? String
        }
        
        if let id = owner.objectForKey("id"){
            myOwner.id = id as? Int
        }
        
        if let login = owner.objectForKey("login"){
            myOwner.login = login as? String
        }
        
        if let url = owner.objectForKey("url"){
            myOwner.url = url as? String
        }
        
        return myOwner
        
    }

}
