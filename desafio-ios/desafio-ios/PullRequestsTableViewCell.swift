//
//  PullRequestsTableViewCell.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class PullRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPullRequestTitle: UILabel!
    @IBOutlet weak var lblPullRequestBody: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var aiLoader: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWithPullRequest(pullRequest:PullRequest){
        
        self.lblUserName.text = pullRequest.user?.login
        self.lblPullRequestBody.text = pullRequest.body
        self.lblPullRequestTitle.text = pullRequest.title?.uppercaseString
        self.lblDate.text = "Updated at \(Util.convertDateFormater((pullRequest.updated_at!), format: "dd/MM/yyyy"))"
        
        self.imgUser.image = nil
        
        self.aiLoader.startAnimating()
        self.aiLoader.hidden = false
        
        Util.loadImageFromStringUrl((pullRequest.user?.avatar_url)!, completion: { (imageView) in
            
            self.aiLoader.stopAnimating()
            self.aiLoader.hidden = true
            
            dispatch_async(dispatch_get_main_queue(), {
                self.imgUser.image = imageView.image
            })
            
        })
    
    }

}
