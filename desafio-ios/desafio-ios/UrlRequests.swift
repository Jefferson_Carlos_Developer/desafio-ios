//
//  UrlRequests.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class UrlRequests {
    
    private func getBaseUrl() -> String{
        return "https://api.github.com/"
    }
    
    private func getBaseUrlWithSufix(sufix:String) -> String{
        return getBaseUrl()+sufix
    }
    
    func urlApiRetrieveRepositories() -> String{
        return self.getBaseUrlWithSufix("search/repositories")
    }
    
    func urlApiRetrievePullRequets(userName:String, repositoryName:String) -> String{
        return getBaseUrlWithSufix("repos/\(userName)/\(repositoryName)/pulls")
    }
    
    
}
