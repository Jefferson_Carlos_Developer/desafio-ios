//
//  Util.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import AlamofireImage

class Util {
    
    static func loadImageFromStringUrl(strUrl:String, completion:(imageView:UIImageView) -> Void){
        
        let imageView = UIImageView()
        
        imageView.af_setImageWithURL(
            
            NSURL(string: strUrl)!,
            placeholderImage: UIImage(named:""),
            filter: nil,
            imageTransition: .CrossDissolve(1.0),
            completion: { response in
                completion(imageView: imageView)
            }
        )
        
    }
    
    static func makeCommunication(str:String){
        
        if let url = NSURL(string: str) {
            let application = UIApplication.sharedApplication()
            
            if application.canOpenURL(url) {
                application.openURL(url)
            }
            else{
                NSLog("Error")
            }
        }
        
    }
    
    static func convertDateFormater(date: String, format:String) -> String {
        
        let data = date.characters.split{$0 == "T"}.map(String.init)
        let sData = data[0].characters.split{$0 == "-"}.map(String.init)
        
        return "\(sData[1])/\(sData[2])/\(sData[0])"
        
    }
    
    static func showAlert(vc:UIViewController, title: String, message:String){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        
        alertController.addAction(defaultAction)
        
        vc.presentViewController(alertController, animated: true, completion: nil)
    }
    
}
