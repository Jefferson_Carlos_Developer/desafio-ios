//
//  RepositoryTask.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import Alamofire

class RepositoryTask {
    
    private var page:Int!
    private var repositoryParser = RepositoryParser()
    
    func retrieveRepositoriesHandler(parameters:[String : AnyObject], handler:(status: Bool, result: Repository?, error: ErrorType!) -> Void ) {

        let urlRequests = UrlRequests()
        let url = NSURL(string: urlRequests.urlApiRetrieveRepositories())
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON { (response) -> Void in
            
            if(response.1?.statusCode == 200){
                
                var repository = Repository()
                repository = self.repositoryParser.parserRepositoryObject(response.2.value!)
                handler(status: true, result: repository, error: nil)

            }else{
                
                handler(status: false, result: nil, error: response.2.error)
            
            }
        
        }
        
    }

}
