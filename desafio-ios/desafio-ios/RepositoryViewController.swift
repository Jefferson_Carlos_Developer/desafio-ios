//
//  RepositoryViewController.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import Alamofire

class RepositoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    //Properties
    private var repository:Repository!
    private var repositoryParser:RepositoryParser!
    private var page:Int!
    private let active = UIActivityIndicator()
    private let canLoadMore:Bool! = true
    private var selectedIndex:Int!
    private let repositoryTask = RepositoryTask()
    
    //@IBOutlets
    @IBOutlet weak var tvRepository: UITableView!
    
    //MARK:LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.repositoryParser = RepositoryParser()
        self.repository = Repository()
        
        repositoryParser.myRepository = self.repository
        
        self.tvRepository.delegate = self
        self.tvRepository.dataSource = self

        self.page = 30
        self.loadContent()
        
        //Remove empty cells
        self.tvRepository.tableFooterView = UIView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        loadContent()
    }
    
    //MARK:Functions
    
    //Call it to get github repositories with pagination.
    func loadContent() {
        
        self.active.showActivityIndicator((self.navigationController?.view)!)
        print("PAGE \(page)")
        
        let parameters:[String : AnyObject]!
        parameters = ["q": "language:Java", "sort":"stars", "page":self.page]

        repositoryTask.retrieveRepositoriesHandler(parameters) { (status, result, error) in
            
            self.active.hideActivityIndicator((self.navigationController?.view)!)
            
            if (status == true) {
                
                self.page = self.page + 1
                self.repository = result
                self.tvRepository.reloadData()
                
            }else{
                
                if(error != nil){
                    Util.showAlert(self,title: NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String,message: "Houve um problema!\nTente Novamente mais tarde.")
                }
                
            }
        }
        
    }
    
    //MARK: TableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.repository != nil && repository.item != nil){
            return self.repository.item!.count
        }else{
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let repositoryItem = repository.item![indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! RepositoryTableViewCell
        
        cell.configureCellWithRepositoryItem(repositoryItem)
        cell.selectionStyle = .None
        
        if(indexPath.row == repository.item!.count-1){
            self.loadContent()
        }
         
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedIndex = indexPath.row
        self.performSegueWithIdentifier("goToPullRequests", sender: self)
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        cell.contentView.layer.opacity = 0.0
        UIView.animateWithDuration(0.5) { 
            cell.contentView.layer.opacity = 1
        }
        
    }
    
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "goToPullRequests" {
            let vc = segue.destinationViewController as! PullRequestsViewController
            vc.repository = self.repository.item![self.selectedIndex].name
            vc.user = self.repository.item![self.selectedIndex].owner!.login
            
        }
     }

}
