//
//  PullRequestUserParser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class PullRequestUserParser: NSObject {
    
    func parserPullRequestObject(pullRequestUser: AnyObject ) -> PullRequestUser{
        
        let myPullRequestUser = PullRequestUser()
        
        if let id = pullRequestUser.objectForKey("id"){
            myPullRequestUser.id = id as? Int
        }
        
        if let login = pullRequestUser.objectForKey("login"){
            myPullRequestUser.login = login as? String
        }
        
        if let avatar_url = pullRequestUser.objectForKey("avatar_url"){
            myPullRequestUser.avatar_url = avatar_url as? String
        }
        
        return myPullRequestUser

    
    }

}
