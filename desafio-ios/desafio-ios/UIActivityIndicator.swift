//
//  UIActivityIndicator.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 11/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class UIActivityIndicator: UIView {

    var container: UIView = UIView()
    var mask: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // MARK: - LOADING
    func showActivityIndicator(uiView: UIView) {
        
        uiView.userInteractionEnabled = false
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.clearColor()
        container.alpha = 0
        
        mask.frame = uiView.frame
        mask.center = uiView.center
        mask.backgroundColor = UIColor.blackColor()
        mask.alpha = 0.5
        
        loadingView.frame = CGRectMake(0, 0, 80, 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.blackColor()
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 5
        
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        activityIndicator.center = CGPointMake(loadingView.frame.size.width / 2, loadingView.frame.size.height / 2);
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(mask)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
        
        UIView.animateWithDuration(0.2) {
            self.container.alpha = 1
        }
    }
    
    func hideActivityIndicator(uiView: UIView) {
        
        UIView.animateWithDuration(0.2, animations: {
            self.container.alpha = 0
            
        }) { (flag) in
            self.activityIndicator.stopAnimating()
            self.container.removeFromSuperview()
            uiView.userInteractionEnabled = true
        }
        
    }

}
