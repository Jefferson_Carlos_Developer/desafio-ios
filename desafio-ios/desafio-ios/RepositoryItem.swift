//
//  RepositoryItem.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 10/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class RepositoryItem: NSObject {
    
    var itemDescription:String?
    var forks_count:Int?
    var id:Int?
    var name:String?
    var owner:Owner?
    var stargazers_count:Int?
    var url:String?

}
