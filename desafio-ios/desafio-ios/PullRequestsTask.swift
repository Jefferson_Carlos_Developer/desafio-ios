//
//  PullRequestsTask.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 12/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit
import Alamofire

class PullRequestsTask {
    
    private var page:Int!
    private var pullRequestParser = PullRequestParser()
    
    func retrieveRepositoriesHandler(userName:String, repositoryName:String, handler:(status:Bool, result: [PullRequest]?, error: NSError!) -> Void ) {
        
        let urlRequests = UrlRequests()
        let url = NSURL(string: urlRequests.urlApiRetrievePullRequets(userName, repositoryName: repositoryName))
        var arrayData = [PullRequest]()
        
        Alamofire.request(.GET, url!).responseJSON { (response) -> Void in
            
            if(response.1?.statusCode == 200){
                
                for pullRequest in response.2.value as! NSArray{
                    arrayData.append(self.pullRequestParser.parserPullRequestObject(pullRequest))
                }
                
                handler(status:true, result: arrayData, error: nil)
                
            }else{
                
                handler(status: false, result: nil, error: nil)
                
            }
            
        }
        
    }
}
