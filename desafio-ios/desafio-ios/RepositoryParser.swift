//
//  RepositoryParser.swift
//  Desafio-iOS
//
//  Created by jeffersoncsilva on 10/01/17.
//  Copyright © 2017 JeffersonCarlos. All rights reserved.
//

import UIKit

class RepositoryParser: NSObject {
    
    var myRepository = Repository()
    
    func parserRepositoryObject(repository: AnyObject ) -> Repository{
        
        if(myRepository.item == nil){
            myRepository.item = []
        }
        
        if let incomplete_results = repository.objectForKey("incomplete_results"){
            myRepository.incomplete_results = incomplete_results as? Int
        }
        
        if let total_count = repository.objectForKey("total_count"){
            myRepository.total_count = total_count as? Int
        }
        
        if let items = repository.objectForKey("items"){

            for item in items as! NSArray{
                
                let repositoryItem = RepositoryItemParser().parserRepositoryItemObject(item)
                myRepository.item?.append(repositoryItem)
                
            }
            
        }
        
        return myRepository
    }

}
